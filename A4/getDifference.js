function getDifference(a, b) {
  if (b > a) {
    [a, b] = [b, a]; 
  }
  return a - b;
}
console.log(getDifference(5, 3))
console.log(getDifference(5, 8))
