body{
    font-family:Arial, Helvetica, sans-serif;
}

caption{
    color: rgb(0, 0, 49);
    font-weight: bolder;
    font-size: 30px;
}


[type="text"]{
    width: 400px;
    height: 30px;
    border: 2px solid rgb(182, 180, 180);
    border-radius: 3px;
}

[placeholder]{
    padding-left: 40px;
    font-weight: 540;
}

[type="button"]{
    width: 100%;
    height: 30px;
    background-color:  rgb(0, 0, 49);
    color: white;
    font-size: 16px;
    border-radius: 3px;
    cursor: pointer;
}

label{
    position: absolute;
    left: 20px;
    font-size: 10px;
    padding: 11px;
    height: max-content;
    background-color: rgb(220, 219, 219);
    border-right: 2px solid rgb(175, 172, 172);
}

[type="button"]:hover{
    background-color:  rgb(31, 31, 61);
}

.dollar{
    top: 66px;
}

.percent{
    top: 112.5px;
    height: 10.5px;
}

.years [placeholder]{
    padding-left: 10px;
}

.years [type="text"]{
    width: 96.5%;
}


.monthly{
    top: 322px;
    height: 10px;
}

.total{
    top: 369px;
    height: 10px;
}

.totalinterest{
    top: 416px;
    height: 10px;
}


.resulttable [type="text"]{
    width:330px;
    padding-left: 110px;
}

.resulttable .resultpay{
    padding-left: 100px;
    padding-right: 11px;
}

.resulttable .resultint{
    padding-left: 90px;
    padding-right: 21px;
}
